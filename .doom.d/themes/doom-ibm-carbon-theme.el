;;;; doom-ibm-carbon-theme.el --- inspired by VIM Challenger Deep -*- no-byte-compile: t; -*-
(require 'doom-themes)

;;
(defgroup doom-ibm-carbon-theme nil
  "Options for doom-themes"
  :group 'doom-themes)

(defcustom doom-ibm-carbon-brighter-modeline nil
  "If non-nil, more vivid colors will be used to style the mode-line."
  :group 'doom-ibm-carbon-theme
  :type 'boolean)

(defcustom doom-ibm-carbon-brighter-comments nil
  "If non-nil, comments will be highlighted in more vivid colors."
  :group 'doom-ibm-carbon-theme
  :type 'boolean)

(defcustom doom-ibm-carbon-comment-bg doom-ibm-carbon-brighter-comments
  "If non-nil, comments will have a subtle, darker background. Enhancing their
legibility."
  :group 'doom-ibm-carbon-theme
  :type 'boolean)

(defcustom doom-ibm-carbon-padded-modeline doom-themes-padded-modeline
  "If non-nil, adds a 4px padding to the mode-line. Can be an integer to
determine the exact padding."
  :group 'doom-ibm-carbon-theme
  :type '(choice integer boolean))

;;
(def-doom-theme doom-ibm-carbon
  "A dark theme inspired by VIM Challenger Deep"

  ;; TODO: Update base colors
  ;; name        default   256       16
  ((bg         '("#101010" "#171717" nil            )) ;; DONE
   (bg-alt     '("#161616" "#1A1A1A" nil            )) ;; DONE
   (base0      '("#100E23" "#080808" "black"        ))
   (base1      '("#292F37" "#262626" "brightblack"  ))
   (base2      '("#3D4551" "#3A3A3A" "brightblack"  ))
   (base3      '("#4C4B68" "#444466" "brightblack"  ))
   (base4      '("#565575" "#555577" "brightblack"  ))
   (base5      '("#858FA5" "#8888AA" "brightblack"  ))
   (base6      '("#9BA7BF" "#99AABB" "brightblack"  ))
   (base7      '("#B0BED8" "#BBBBDD" "brightblack"  ))
   (base8      '("#BAC9E4" "#BBCCEE" "white"        ))
   (fg-alt     '("#F4F4F4" "#F4F4FF" "brightwhite"  )) ;; DONE
   (fg         '("#FAFAFA" "#FFFFFF" "white"        )) ;; DONE

   ;; TODO: Update green, blue, brightcyan, and cyan
   (grey       base4)
   (red        '("#FA75AB" "#FA77AA" "red"          )) ;; DONE
   (orange     '("#FF832B" "#FF8822" "brightred"    )) ;; DONE
   (green      '("#95FFA4" "#99FFAA" "green"        ))
   (teal       '("#92EEEE" "#99EEEE" "brightgreen"  )) ;; DONE
   (yellow     '("#F1C21B" "#FFCC11" "yellow"       )) ;; DONE
   (blue       '("#6EA6FF" "#66AAFF" "brightblue"   )) ;; DONE
   (dark-blue  '("#65B2FF" "#66BBFF" "blue"         ))
   (magenta    '("#BB8EFF" "#BB88FF" "magenta"      )) ;; DONE
   (violet     '("#BB8EFF" "#8833FF" "brightmagenta")) ;; DONE
   (cyan       '("#AAFFE4" "#AAFFEE" "brightcyan"   ))
   (dark-cyan  '("#62D196" "#66DD99" "cyan"   ))

   ;; face categories -- required for all themes
   (highlight      red)
   (vertical-bar base1)
   (selection      violet)
   (builtin        magenta) ;; DONE
   (comments       (if doom-ibm-carbon-brighter-comments dark-blue base4))
   (doc-comments   (if doom-ibm-carbon-brighter-comments (doom-darken dark-cyan 0.3) base5) )
   (constants      red) ;; DONE
   (functions      teal) ;; DONE
   (keywords       magenta) ;; DONE
   (methods        red) ;; ?
   (operators      teal) ;; DONE
   (type           teal) ;; DONE
   (strings        blue)
   (variables      red) ;; DONE
   (numbers        base8) ;; ?
   (region         base2)
   (error          magenta)
   (warning        yellow)
   (success        green)
   (vc-modified    orange)
   (vc-added       green)
   (vc-deleted     red)

   ;; custom categories
   (hidden     `(,(car bg) "black" "black"))
   (-modeline-bright doom-ibm-carbon-brighter-modeline)
   (-modeline-pad
    (when doom-ibm-carbon-padded-modeline
      (if (integerp doom-ibm-carbon-padded-modeline) doom-ibm-carbon-padded-modeline 4)))

   (modeline-fg     nil)
   (modeline-fg-alt base5)

   (modeline-bg
    (if -modeline-bright
        base3
      `(,(doom-darken (car bg) 0.15) ,@(cdr base0))))
   (modeline-bg-l
    (if -modeline-bright
        base3
      `(,(doom-darken (car bg) 0.1) ,@(cdr base0))))
   (modeline-bg-inactive   (doom-darken bg 0.1))
   (modeline-bg-inactive-l `(,(car bg) ,@(cdr base1))))


  ;; --- extra faces ------------------------
  (((secondary-selection &override) :background base0)
   (elscreen-tab-other-screen-face :background "#353a42" :foreground "#1e2022")

   ((line-number &override) :foreground base4)
   ((line-number-current-line &override) :foreground fg)

   (font-lock-comment-face
    :foreground comments
    :background (if doom-ibm-carbon-comment-bg (doom-lighten bg 0.05)))
   (font-lock-doc-face
    :inherit 'font-lock-comment-face
    :foreground doc-comments)

   (doom-modeline-bar :background (if -modeline-bright modeline-bg highlight))

   (mode-line
    :background modeline-bg :foreground modeline-fg
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg)))
   (mode-line-inactive
    :background modeline-bg-inactive :foreground modeline-fg-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive)))
   (mode-line-emphasis
    :foreground (if -modeline-bright base8 highlight))
   (header-line :inherit 'mode-line :background "#23214b")

   (solaire-mode-line-face
    :inherit 'mode-line
    :background modeline-bg-l
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-l)))
   (solaire-mode-line-inactive-face
    :inherit 'mode-line-inactive
    :background modeline-bg-inactive-l
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive-l)))

   ;; --- major-mode faces -------------------
   ;; css-mode / scss-mode
   (css-proprietary-property :foreground orange)
   (css-property             :foreground green)
   (css-selector             :foreground blue)

   ;; markdown-mode
   (markdown-markup-face :foreground base5)
   (markdown-header-face :inherit 'bold :foreground red)
   ((markdown-code-face &override) :background (doom-lighten base3 0.05))

   ;; outline (affects org-mode)
   ((outline-1 &override) :foreground blue :background nil)

   ;; org-mode
   ((org-block &override) :background base1)
   ((org-block-begin-line &override) :background base1 :foreground comments)
   (org-hide :foreground hidden)
   (org-link :foreground orange :underline t :weight 'bold)
   (solaire-org-hide-face :foreground hidden)

   ;; tooltip
   (tooltip              :background base0 :foreground fg))

  ;; --- extra variables ---------------------
  ;; ()
  )

;;; doom-ibm-carbon-theme.el ends here;; ~/.doom.d/themes/doom-ibm-carbon-theme.el -*- lexical-binding: t; -*-
