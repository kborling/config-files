;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; TODO $> doom refresh

;; These are used for a number of things, particularly for GPG configuration,
;; some email clients, file templates and snippets.
(setq user-full-name "Kevin Borling"
      user-mail-address "kborling@protonmail.com")

;; Font Settings
;; Fura Code Nerd Font (Alternative)
;; (setq doom-font (font-spec :family "Fura Code Nerd Font" :size 30)
(setq doom-font (font-spec :family "Fira Code Retina" :size 30)
      ;; doom-variable-pitch-font (font-spec :family "Input Mono Light" :height 60)
      doom-variable-pitch-font (font-spec :family "Fira Code Retina" :height 60)
      ;; doom-variable-pitch-font (font-spec :family "IBM Plex Serif Light" :height 60)
      ;; doom-unicode-font (font-spec :family "DejaVu Sans Mono")
      doom-unicode-font (font-spec :family "Fira Code Retina")
      ;; doom-serif-font (font-spec :family "IBM Plex Serif" :weight 'semi-bold :width 'extra-condensed)
      doom-serif-font (font-spec :family "IBM Plex Serif Light" :size 30)
      ;; doom-big-font (font-spec :family "Input Mono Light" :size 38))
      doom-big-font (font-spec :family "Fira Code Retina" :size 32))

;; doom-unicode-font
(setq display-line-numbers-width 4)
(setq-default line-spacing 4)

; Disable line numbers for some modes
(dolist (mode '(org-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;; disable line mode
;; (setq hl-line-mode nil)

;; (set-face-bold-p 'bold nil)

;; Theme Settings
(setq doom-themes-treemacs-theme "doom-colors") ; use the colorful treemacs theme

(add-to-list 'load-path "~/.doom.d/themes/")
(add-to-list 'custom-theme-load-path "~/.doom.d/themes")

;; (load-theme 'gruber-darker t)
;; (setq doom-theme 'doom-horizon)
(setq doom-theme 'doom-dracula)

(doom-themes-visual-bell-config)
(doom-themes-treemacs-config)
(doom-themes-org-config)

(after! doom-themes
  (setq doom-themes-enable-bold t
   doom-themes-enable-italic t))

;; Banners
(setq +doom-dashboard-banner-dir
      (concat doom-private-dir "banners/"))
;; Banner padding
(setq +doom-dashboard-banner-padding '(1 . 3))

;; Modeline Settings
(after! doom-modeline
  ;; (setq doom-modeline-major-mode-color-icon t)
  (setq doom-modeline-gnus t
        doom-modeline-gnus-timer 'nil)
  (setq doom-modeline-bar-width 3)
  (setq doom-modeline-height 15)
  (setq doom-modeline-github t)
  (setq doom-modeline-version nil))

;; Make compatible with tiling wm
(setq frame-resize-pixelwise t)
;; Show trailing whitespace
(setq show-trailing-whitespace t)

;; Editing Preferences
;; (setq display-line-numbers-type 'relative)
;; (setq display-line-numbers-type nil)
(setq-default fill-column 80)

;; Reduce delay of which key
(after! which-key
  (setq which-key-idle-delay 0.5
        which-key-idle-secondary-delay 0.01
        which-key-sort-order 'which-key-key-order-alpha))

;; Reduce delay of company
(after! company
  (setq company-idle-delay 0))

;; Set default browser
(setq browse-url-browser-function 'browse-url-firefox)
;; (setq browse-url-browser-function 'eww-browse-url)

;; Easy window navigation
(map!
 (:after evil
  :en "C-h"   #'evil-window-left
  :en "C-j"   #'evil-window-down
  :en "C-k"   #'evil-window-up
  :en "C-l"   #'evil-window-right))
;; Insert new lines above/below
(map!
 (:after evil
  :m  "] SPC" #'evil-motion-insert-newline-below
  :m  "[ SPC" #'evil-motion-insert-newline-above))
;; Dired
(map!
 (:after dired
  (:map dired-mode-map
   "C-SPC" #'peep-dired)))

;; Create new workspace when switching projects
;; (setq +workspaces-on-switch-project-behavior t)

;; Python LSP
(require 'lsp-python-ms)
(add-hook 'python-mode-hook #'lsp) ; or lsp-deferred

;; for executable of language server, if it's not symlinked on your PATH
(setq lsp-python-ms-executable
      "~/python-language-server/output/bin/Release/linux-x64/publish/Microsoft.Python.LanguageServer")

;; Rust
(require 'rustic)
;; (add-hook 'rustic-mode-hook #'lsp)
(after! rustic
  ;; Use rust-analyzer instead of RLS
  ;; (setq rustic-lsp-server 'rust-analyzer)
  ;; (setq lsp-rust-analyzer-server-command '("~/.cargo/bin/rust-analyzer")))
  (setq rustic-lsp-server 'rls))
(add-hook 'rustic-mode-hook #'lsp-ui-doc-mode)

;; Kill spellcheck for Latex (super slow)
(setq-hook! 'LaTeX-mode-hook +spellcheck-immediately nil)

;; Set ripgrep as default for ivy
(setq +ivy-project-search-engines '(rg))

;; Org
(after! org (set-popup-rule! "^Capture.*\\.org$" :side 'right :size .40 :select t :vslot 2 :ttl 3))
(after! org (set-popup-rule! "Dictionary" :side 'bottom :height .40 :width 20 :select t :vslot 3 :ttl 3))
;; (after! org (set-popup-rule! "*helm*" :side 'bottom :height .40 :select t :vslot 5 :ttl 3))
(after! org (set-popup-rule! "*ivy*" :side 'bottom :height .40 :select t :vslot 5 :ttl 3))
(after! org (set-popup-rule! "*deadgrep" :side 'bottom :height .40 :select t :vslot 4 :ttl 3))
(after! org (set-popup-rule! "*xwidget" :side 'right :size .40 :select t :vslot 5 :ttl 3))
(after! org (set-popup-rule! "*org agenda*" :side 'right :size .40 :select t :vslot 2 :ttl 3))


(global-auto-revert-mode t)
;; Org Directory Settings
(setq org-directory "~/.org/")
(setq diary-file "~/.org/diary.org")

(after! org (setq org-directory "~/.org/"
                  org-image-actual-width nil
                  +org-export-directory "~/.export/"
                  org-archive-location "~/.org/archive.org::datetree/"
                  org-default-notes-file "~/.org/refile.org"
                  projectile-project-search-path '("~/")))

(after! org (setq org-agenda-diary-file "~/.org/diary.org"
                  org-agenda-use-time-grid nil
                  org-agenda-skip-scheduled-if-done t
                  org-agenda-skip-deadline-if-done t
                  org-habit-show-habits t))

;; Org Export Options
(after! org (setq org-html-head-include-scripts t
                  org-export-with-toc t
                  org-export-with-author t
                  org-export-headline-levels 5
                  org-export-with-drawers t
                  org-export-with-email t
                  org-export-with-footnotes t
                  org-export-with-latex t
                  org-export-with-section-numbers nil
                  org-export-with-properties t
                  org-export-with-smart-quotes t
                  ;; org-export-backends '(pdf ascii html latex odt pandoc)
                  ))

;; Org Keyword Sequence
(after! org (setq org-todo-keywords
                  (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)" "FIXME(f)" "DEBUG(b)")
                          (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)" "PHONE" "MEETING")))))

;; Org Keywords
(after! org (setq org-todo-keyword-faces
                  '(("TODO" :foreground "tomato" :weight bold)
                    ("NEXT" :foreground "violet red" :weight bold)
                    ("DONE" :foreground "slategrey" :weight bold)
                    ("WAITING" :foreground "light sea green" :weight bold)
                    ("HOLD" :foreground "#1E90FF" :weight bold)
                    ("CANCELLED" :foreground "#FF4500" :weight bold)
                    ("MEETING" :foreground "green" :weight bold)
                    ("PHONE" :foreground "green" :weight bold)
                    ("FIXME" :foreground "tomato" :weight bold)
                    ("DEBUG" :foreground "#A020F0" :weight bold))))

;; Org Filter Tasks
(after! org (setq org-todo-state-tags-triggers
                  (quote (("CANCELLED" ("CANCELLED" . t))
                          ("WAITING" ("WAITING" . t))
                          ("HOLD" ("WAITING") ("HOLD" . t))
                          (done ("WAITING") ("HOLD"))
                          ("TODO" ("WAITING") ("CANCELLED") ("HOLD"))
                          ("NEXT" ("WAITING") ("CANCELLED") ("HOLD"))
                          ("DONE" ("WAITING") ("CANCELLED") ("HOLD"))))))

;; Better list bullets
(font-lock-add-keywords 'org-mode
                        '(("^ *\\([-]\\) "
                           (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

(after! org (setq org-startup-indented t
                  org-hide-emphasis-markers t
                  org-src-tab-acts-natively t))
(add-hook 'org-mode-hook 'variable-pitch-mode)
(add-hook 'org-mode-hook 'visual-line-mode)
(add-hook 'org-mode-hook 'org-num-mode)
;; (add-hook 'org-mode-hook 'display-line-numbers-mode nil)

;; Org Refile

;; Targets include this file and any file contributing to the agenda - up to 9 levels deep
(setq org-refile-targets (quote ((nil :maxlevel . 9)
                                 (org-agenda-files :maxlevel . 9))))

;; Use full outline paths for refile targets - we file directly with IDO
(setq org-refile-use-outline-path t)

;; Targets complete directly with IDO
(setq org-outline-path-complete-in-steps nil)

;; Allow refile to create parent tasks with confirmation
(setq org-refile-allow-creating-parent-nodes (quote confirm))

;; Use the current window for indirect buffer display
(setq org-indirect-buffer-display 'current-window)

;; Org Source Block (Fix background highlighting color issue with certain themes)
;; (custom-set-faces
;;  '(org-block-begin-line
;;    ((t (:underline "#2d2e30" :foreground "#ebc390" :background "#2d2e30"))))
;;  '(org-block
;;    ((t (:background "#2d2e30"))))
;;  '(org-block-end-line
;;    ((t (:overline "#2d2e30" :foreground "#ebc390" :background "#2d2e30"))))
;;  )

;; Org Agenda
(setq org-agenda-files (list "~/.org/todo.org"))
;; Do not dim blocked tasks
(setq org-agenda-dim-blocked-tasks nil)

;; Compact the block agenda view
(setq org-agenda-compact-blocks t)

;; I want to see deadlines in the agenda 30 days before the due date.
(setq org-deadline-warning-days 30)

;; Org-mode can export tables as TAB or comma delimited formats. I set the default format to CSV with:
(setq org-table-export-default-format "orgtbl-to-csv")

;; Add new line at the end when saving
(setq require-final-newline t)
;; Better Org Lists
(setq org-list-demote-modify-bullet (quote (("+" . "-")
                                            ("*" . "-")
                                            ("1." . "-")
                                            ("1)" . "-")
                                            ("A)" . "-")
                                            ("B)" . "-")
                                            ("a)" . "-")
                                            ("b)" . "-")
                                            ("A." . "-")
                                            ("B." . "-")
                                            ("a." . "-")
                                            ("b." . "-"))))
                                        ; Org Fontify (remove fontify error)
(setq org-src-fontify-natively nil)
                                        ; Prefer lower of uppercase
;; markdown improvements
(map! :localleader
      :map markdown-mode-map
      :prefix ("i" . "Insert")
      :desc "Blockquote"    "q" 'markdown-insert-blockquote
      :desc "Bold"          "b" 'markdown-insert-bold
      :desc "Code"          "c" 'markdown-insert-code
      :desc "Emphasis"      "e" 'markdown-insert-italic
      :desc "Footnote"      "f" 'markdown-insert-footnote
      :desc "Code Block"    "s" 'markdown-insert-gfm-code-block
      :desc "Image"         "i" 'markdown-insert-image
      :desc "Link"          "l" 'markdown-insert-link
      )

;; Turn on `display-time-mode' if you don't use an external bar.
(setq display-time-default-load-average nil)
(display-time-mode t)

(require 'gdscript-mode)
(defun lsp--gdscript-ignore-errors (original-function &rest args)
  "Ignore the error message resulting from Godot not replying to the `JSONRPC' request."
  (if (string-equal major-mode "gdscript-mode")
      (let ((json-data (nth 0 args)))
        (if (and (string= (gethash "jsonrpc" json-data "") "2.0")
                 (not (gethash "id" json-data nil))
                 (not (gethash "method" json-data nil)))
            nil ; (message "Method not found")
          (apply original-function args)))
    (apply original-function args)))
;; Runs the function `lsp--gdscript-ignore-errors` around `lsp--get-message-type` to suppress unknown notification errors.
(advice-add #'lsp--get-message-type :around #'lsp--gdscript-ignore-errors)
