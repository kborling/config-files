(define-package "evil-collection" "20201012.1429" "A set of keybindings for Evil mode"
  '((emacs "25.1")
    (evil "1.2.13")
    (annalist "1.0"))
  :commit "6f7fbac3de44049040a5b060ff82d308d2de5918" :keywords
  '("evil" "tools")
  :authors
  '(("James Nguyen" . "james@jojojames.com"))
  :maintainer
  '("James Nguyen" . "james@jojojames.com")
  :url "https://github.com/emacs-evil/evil-collection")
;; Local Variables:
;; no-byte-compile: t
;; End:
