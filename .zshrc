export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

export NODE_PATH=$NODE_PATH:`npm root -g`
export PATH="node_modules/.bin":$PATH
# Allow user-wide installations
export PATH="$HOME/.node_modules/bin:$PATH"
export PATH="$HOME/.local/bin:$PATH"
export XDG_CONFIG_HOME=~/.config
export GOPATH=$HOME/go
export RUST_SRC_PATH=/lib/rustlib/src/rust/src

if [ -n "$DISPLAY" ]; then
    export BROWSER=firefox
else
    export BROWSER=chromium
fi

export OLD_SHELL=/bin/sh

# pass
export PASSWORD_STORE_ENABLE_EXTENSIONS=true

# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory autocd extendedglob nomatch notify
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/kevin/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
fpath=($fpath "/home/kevin/.zfunctions")

# Set Spaceship ZSH as a prompt
autoload -U promptinit; promptinit
prompt spaceship

plugins=(git colored-man colorize github vi-mode virtualenv pip python zsh-syntax-highlighting)

# why would you type 'cd dir' if you could just type 'dir'?
setopt AUTO_CD
# Now we can pipe to multiple outputs!
setopt MULTIOS
# Spell check commands!  (Sometimes annoying)
setopt CORRECT
# This makes cd=pushd
setopt AUTO_PUSHD
# This will use named dirs when possible
setopt AUTO_NAME_DIRS
# If we have a glob this will expand it
setopt GLOB_COMPLETE
setopt PUSHD_MINUS
# blank pushd goes to home
setopt PUSHD_TO_HOME
# this will ignore multiple directories for the stack.  Useful?  I dunno.
setopt PUSHD_IGNORE_DUPS
# 10 second wait if you do something that will delete everything.  I wish I'd had this before...
setopt RM_STAR_WAIT
setopt NO_HUP

bindkey -M viins '\C-i' complete-word

# Faster! (?)
zstyle ':completion::complete:*' use-cache 1

# Errors format
zstyle ':completion:*:corrections' format '%B%d (errors %e)%b'
# i3
alias i3config='emacs ~/.config/i3/config'
alias xconfig='emacs ~/.xmonad/xmonad.hs'
alias xbconfig='emacs ~/.xmobarrc'

# doom emacs
# alias doom='~/.emacs.d/bin/doom'
alias doom='~/doom-emacs/bin/doom'

# dotnet
alias dotnet='~/.dotnet/dotnet'

# lynx
alias lynx='lynx -cfg=~/.lynx.cfg -lss=~/.lynx.lss -vikeys'

# cp
alias cp='cp -R'

# scp
alias scp='scp -r'
alias grep='rg'

# faster du
alias du='dust'

# Changing "ls" to "exa"
alias ls='exa -al --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT --color=always --group-directories-first' # tree listing
alias l.='exa -a | egrep "^\."'

# ça pipe
alias -g G='| grep'
alias -g L='| less'
alias -g M='| most'
alias -g NE='2> /dev/null'
alias -g NUL='> /dev/null 2>&1'
alias -g S='| sort'
alias -g T='tail -f'
alias -g W='| wc -l'

# cd
alias ..='cd ..'
alias s='cd ..'
alias d='cd -'

# mkdir
alias mkdir='mkdir -p'

# df
alias df='df -kTh'

# emacs
alias em="/usr/bin/emacs -nw"
alias emacs="emacsclient -c -a 'emacs'"
alias demacs="/usr/bin/emacs"
export ALTERNATE_EDITOR=""                # setting for emacsclient
export EDITOR="emacsclient -t -a ''"      # $EDITOR use Emacs in terminal
export VISUAL="emacsclient -c -a emacs"   # $VISUAL use Emacs in GUI mode

# vim
# EDITOR=nvim
alias vi='nvim'
alias vim='nvim'
alias neo='neovide & disown'
alias vimrc='nvim ~/.config/nvim/init.vim'

# git
alias g='git'

# pacman
alias pacsy='sudo pacman -Sy'
alias pacsyu='sudo pacman -Syu'
alias pacsu='sudo pacman -Su'
alias pacr='sudo pacman -Rcs'
alias update='sudo pacman -Syu && yay -Syu'
alias unlock="sudo rm /var/lib/pacman/db.lck"    # remove pacman lock
alias cleanup='sudo pacman -Rns $(pacman -Qtdq)' # remove orphaned packages

# get fastest mirrors
alias mirror="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
alias mirrord="sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist"
alias mirrors="sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist"
alias mirrora="sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist"

# confirm before overwriting something
alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'

# ping
alias ping='ping -c 3'

# sudo
alias s='sudo '
alias se='sudoedit'

# clear
alias c='clear'

alias gs="git status" # screw you ghostscript
alias gac="git add . && git commit -m" # + commit message
alias gi="git init && gac 'Initial commit'"
alias gp="git push" # + remote & branch names
alias gl="git pull" # + remote & branch names
# Pushing/pulling to origin remote
alias gpo="git push origin" # + branch name
alias glo="git pull origin" # + branch name

# Pushing/pulling to origin remote, master branch
alias gpom="git push origin master"
alias glom="git pull origin master"
alias gb="git branch" # + branch name
alias gc="git checkout" # + branch name
alias gcb="git checkout -b" # + branch name
# FileSearch
function f() { find . -iname "*$1*" ${@:2} }

function r() { grep "$1" ${@:2} -R . }

function ff() { firefox -search "$@" }

#mkdir and cd
function mkcd() { mkdir -p "$@" && cd "$_"; }

# tmux
alias tmuxconfig="emacs ~/.tmux.conf";
# Aliases
alias cppcompile='c++ -std=c++11 -stdlib=libc++'
alias gppcompile='g++ -std=c++11 -stdlib=libc++'

# Use vim for editing config files
alias zshconfig="emacs ~/.zshrc"
alias envconfig="emacs ~/Projects/config/env.sh"

# spotify (HIDPI)
alias spotify="spotify --force-device-scale-factor=1.5"

# protonvpn
alias vpninit="sudo protonvpn init"
alias vpnconnect="sudo protonvpn c"
alias vpnconnectf="sudo protonvpn -f"
alias vpnconnectr="sudo protonvpn -r"
alias vpnconnectp="sudo protonvpn -p2p"
alias vpndisconnect="sudo protonvpn d"

alias ssh="TERM=xterm-color ssh"

alias sshkey="cat ~/.ssh/id_rsa.pub | xclip -sel clip"

alias p="cd ~/Projects"

alias rickroll="curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash"

alias ghost="~/.nvm/versions/node/v10.19.0/lib/node_modules/ghost-cli/bin/ghost"

# wifi
alias wifi="nmcli device wifi connect"

# virtulbox
alias school="ssh -p 2222 mininet@127.0.0.1"
alias schoolftp="sftp -o Port=2222 mininet@127.0.0.1"
alias project1="ssh -p 3333 project1@127.0.0.1"
alias project4ftp="sftp -o Port=3333 debian@127.0.0.1"

source /home/kevin/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
